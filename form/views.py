from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from .models import Programming_Competition, NOSKode
from .forms import Programming_Competition_Form, NOSKode_Form
from django.contrib.auth.decorators import login_required

def home(request):
    template_name = 'index.html'
    return render(request,template_name)

def thanks(request):
    template_name = 'thanks.html'
    context = {'title': 'Programming Competition Form 2017'}
    return render(request,template_name,context)

def programming_Competition(request):
    if request.method =='POST':
        form = Programming_Competition_Form(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.save()
            return redirect('thanks')
    else:
        form = Programming_Competition_Form()
    template_name = 'Programming_Competition.html'
    context = {'form':form, 'title': 'Programming Competition Form 2017'}
    return render(request,template_name,context)

def nOSKode(request):
    if request.method =='POST':
        form = NOSKode_Form(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.save()
            return redirect('thanks')
    else:
        form = NOSKode_Form()
    template_name = 'NOSKode.html'
    context = {'form':form, 'title': 'NOSKode Registration Form 2017'}
    return render(request,template_name,context)

@login_required()
def analytics(request):
    template_name = 'analytics.html'
    program = Programming_Competition.objects.all()
    nosk = NOSKode.objects.all()
    context = {'title': 'Analytics of forms', 'program':program,'nosk':nosk}
    return render (request, template_name, context)
