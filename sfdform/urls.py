"""sfdform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import logout
from form.views import home, thanks, programming_Competition, nOSKode, analytics

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home, name='home'),
    url(r'^Thank_You/$', thanks, name='thanks'),
    url(r'^Programming_Competition/$', programming_Competition, name='programming_Competition'),
    url(r'^NOSKode/$', nOSKode, name='nOSKode'),
    url(r'^analytics/$', analytics, name='analytics'),
    url(r'^analytics/logout/$', logout, name='logout'),
]
